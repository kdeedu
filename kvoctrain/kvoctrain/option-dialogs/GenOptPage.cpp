/***************************************************************************

                       general options dialog page

    -----------------------------------------------------------------------

    begin                : Thu Jun 3 18:08:56 1999

    copyright            : (C) 1999-2001 Ewald Arnold
                           (C) 2001 The KDE-EDU team

    email                : kvoctrain@ewald-arnold.de

    -----------------------------------------------------------------------

 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "GenOptPage.h"
#include <qlineedit.h>
#include <qcheckbox.h>
#include <qbuttongroup.h>
#include <qradiobutton.h>
#include <qvalidator.h>
#include <stdlib.h>
#include <klocale.h>
#include <kdebug.h>

#include <QueryManager.h>


GenOptPage::GenOptPage
(
        int        _btime,
        bool        _smart,
        bool        _autoapply,
	QWidget    *parent,
	const char *name
)
	:
	GenOptPageForm( parent, name )
{
  setCaption(i18n("Options" ));
  //resizer = res;


  group_resize->insert(hb_auto);
  group_resize->insert(hb_percent);
  group_resize->insert(hb_fixed);

  connect( kcfg_autoEntryApply, SIGNAL(toggled(bool)), this, SLOT(slotAutoApplyChecked(bool)) );
  connect( kcfg_smartAppend, SIGNAL(toggled(bool)), SLOT(slotSmartAppend(bool)) );
  connect( c_btime, SIGNAL(toggled(bool)), SLOT(slotBTimeUsed(bool)) );
  //connect( c_saveopt, SIGNAL(toggled(bool)), SLOT(slotAutoSaveOpts(bool)) );
  connect( kcfg_backupTime, SIGNAL(textChanged(const QString&)), SLOT(slotChangeBTime(const QString&)) );
  connect( hb_fixed, SIGNAL(clicked()), SLOT(slotHBfixed()) );
  connect( hb_percent, SIGNAL(clicked()), SLOT(slotHBpercent()) );
  connect( hb_auto, SIGNAL(clicked()), SLOT(slotHBauto()) );

  btime = _btime;
  smart = _smart;
  //autosaveopts = _autosaveopts;
  autoapply = _autoapply;

  kcfg_autoEntryApply->setChecked(autoapply);
  kcfg_smartAppend->setChecked(smart);
  //c_saveopt->setChecked(autosaveopts);

  QString s;
  validator = new QIntValidator (0, 60*60*24*7, 0); // at least once a week

  kcfg_backupTime->setValidator (validator);
  c_btime->setChecked(btime > 0);
  slotBTimeUsed(btime > 0);

  switch (Prefs::headerResizeMode()) {
    case /*kvoctrainView::Automatic*/ 0 :
       hb_auto->setChecked( true );
    break;

    case /*kvoctrainView::Fixed*/ 1 :
       hb_fixed->setChecked( true );
    break;

    case /*kvoctrainView::Percent*/ 2 :
       hb_percent->setChecked( true );
    break;

    default: {
      hb_auto->setChecked( true );
      //resizer = kvoctrainView::Automatic;
    }
  }
}


GenOptPage::~GenOptPage()
{
  delete validator;
}


void GenOptPage::initFocus() const
{
  c_btime->setFocus();
}


void GenOptPage::slotBTimeUsed(bool on)
{
  kcfg_backupTime->setEnabled(on);

  if (on) {
    if (btime < 0 )
      btime = -btime;
    if (btime == 0)
      btime = 10;
  }
  else {
    if (btime > 0 )
      btime = -btime;
  }

  QString s;
  s.setNum (abs(btime));
  bool b = kcfg_backupTime->signalsBlocked();
  kcfg_backupTime->blockSignals(true);
  kcfg_backupTime->setText (s);
  kcfg_backupTime->blockSignals(b);
}


void GenOptPage::slotChangeBTime(const QString& s)
{
   btime = atoi (s.local8Bit());
}


void GenOptPage::slotSmartAppend(bool b)
{
  smart = b;
}


void GenOptPage::slotHBauto()
{
  Prefs::setHeaderResizeMode(Prefs::EnumHeaderResizeMode::Automatic);
}


void GenOptPage::slotHBfixed()
{
  Prefs::setHeaderResizeMode(Prefs::EnumHeaderResizeMode::Fixed);
}


void GenOptPage::slotHBpercent()
{
  Prefs::setHeaderResizeMode(Prefs::EnumHeaderResizeMode::Percent);
}


void GenOptPage::slotAutoApplyChecked(bool ena)
{
  autoapply = ena;
}


void GenOptPage::keyPressEvent( QKeyEvent *e )
{
   if (e->state() & AltButton & ControlButton & ShiftButton == 0) {
     if (  e->key() == Key_Escape )
       emit reject();
     else if (  e->key() == Key_Enter
              ||e->key() == Key_Return)
       emit accept();
     else
       e->ignore();
   }
   else
     e->ignore();
}


#include "GenOptPage.moc"
