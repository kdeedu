/***************************************************************************
                          mainwindow.h  -  description
                             -------------------
    begin                : Sun Oct 21 2001
    copyright            : (C) 2001 by Primoz Anzur
    			   (C) 2004 by Reuben Sutton 
    email                : reubens@gmail.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#define LOCAL_VERSION "2.3.1"
//CVS version changes ONLY with major overhaul OR submission of new features...

#include <kmessagebox.h>
#include <kpopupmenu.h>
#include <khelpmenu.h>
#include <kmainwindow.h>

#include <qpushbutton.h>

#include "kwritewords.h"
#include "kmwview.h"
#include "minparser.h"

class KSelectAction;

/** MainWindow is the base class of the project */
class MainWindow : public KMainWindow
{
  Q_OBJECT
public:
	/** construtor */
	MainWindow();
	/** destructor */
	~MainWindow();

protected:	
  	///Call an instance of the dictionary dialog
  	KWriteWords showConfig;
	///Instance of the config class
	KConfig *config;
	///An instance of the GUI inherited from FrontEnd
	KmwView *m_view;
	
	KSelectAction *levelAct;
	///Helds the current theme name (default or bubbly)
	QString m_themeName;
	///The path to the local data words (local dictionary)
	QString m_setLocalDictionary;
	///All the themes found
	QStringList m_themeString;
	///Set up the actions for the menus
	void setupActions();
	///Set up the settings at start
	void loadSettings();
	///Set the background picture
	void setBackgroundPic();
	///necessary to have the background pic resize correctly
	void resizeEvent(QResizeEvent *);

protected slots:
  	///Opens the dictionary dialog
  	void slot_openBook();
	///Opens the Configure MessedWords dialog
	void optionsPreferences();
	///Apply new settings from Configure KMessedWords dialog to the view
	void updateSettings();
	/**
	     Write the new level in the statusbar
	      @param text the text hat will be written in the statusbar
	 */
	void updateStatusBar(const QString &text);
	///When the user changes the level with the Menu
	void slotChangeLevel();
	void updateButtons();
	void updateButtonText();
};

#endif

