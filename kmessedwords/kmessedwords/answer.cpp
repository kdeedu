/***************************************************************************
                          answer.cpp  -  description
                             -------------------
    begin                : Thu Mar 28 2002
    copyright            : (C) 2002 by Primoz Anzur 
    email                : zerokode@gmx.net
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
 #include <klocale.h>

#include "answer.h"
#include <kdebug.h>
#include <kstandarddirs.h>
#include <kpushbutton.h>
#include <kstdguiitem.h>

/*
 *  Constructs a Answer which is a child of 'parent', with the 
 *  name 'name' and widget flags set to 'f'.
 *
 *  The dialog will by default be modeless, unless you set 'modal' to
 *  TRUE to construct a modal dialog.
 */
Answer::Answer( QWidget* parent,  const char* name, bool modal, WFlags  )
    : QDialog( parent, name, modal, true)
{
    if ( !name )
	setName( "Answer" );
    resize( 256, 256 ); 
    setMinimumSize( QSize( 256, 256 ) );
    setMaximumSize( QSize( 256, 256 ) );
    setCaption( tr2i18n( "Answer" ) );
    AnswerLayout = new QGridLayout( this, 1, 1, 11, 6, "AnswerLayout"); 

    Layout2 = new QHBoxLayout( 0, 0, 6, "Layout2"); 

    btnReplay = new QPushButton( this, "btnReplay" );
    btnReplay->setText( tr2i18n( "&Play Again" ) );
    Layout2->addWidget( btnReplay );

    btnQuit = new KPushButton( this, "btnQuit" );
    btnQuit->setText( tr2i18n( "Quit" ) );
    Layout2->addWidget( btnQuit );

    AnswerLayout->addLayout( Layout2, 1, 0 );
    QSpacerItem* spacer = new QSpacerItem( 20, 20, QSizePolicy::Minimum, QSizePolicy::Expanding );
    AnswerLayout->addItem( spacer, 0, 0 );

    // signals and slots connections
    connect( btnQuit, SIGNAL( clicked() ), this, SLOT( exitAnswer() ) );
    connect( btnReplay, SIGNAL( clicked() ), this, SLOT( replayGame() ) );
}

/*
 *  Destroys the object and frees any allocated resources
 */
Answer::~Answer()
{
    // no need to delete child widgets, Qt does it all for us
}

void Answer::exitAnswer()
{
	if(getAnswer){
		close(); //will show the "OK" button -- Simple close...
	} else {
		emit gameExit();
		close();
	}
}

void Answer::replayGame()
{
	emit gameReplay();
	close();
}

void Answer::doAnswer(bool result){
	kdDebug() << result << endl;	

	if(result){
		btnReplay->setEnabled(true);
		btnQuit->setGuiItem(KStdGuiItem::quit());
		setCaption(i18n("Congratulations!"));                
		setBackgroundPixmap(QPixmap(locate("data","kmessedwords/dudeyes.png")));
		getAnswer=false;
	} else {
		btnReplay->setEnabled(false);
		btnQuit->setGuiItem(KStdGuiItem::ok());
		setCaption(i18n("You Missed!"));
		setBackgroundPixmap(QPixmap(locate("data","kmessedwords/dudeoh.png")));
		getAnswer=true;
	}
}

#include "answer.moc"
