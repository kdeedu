/*
 * Copyright (C) 2001 by Primoz Anzur
		  (C) 2004  Anne-Marie Mahfouf <annma@kde.org>

    This program is free software; you can redistribute it and/or
    modify it under the terms of version 2 of the GNU General Public
    License as published by the Free Software Foundation.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

#ifndef KMWVIEW_H
#define KMWVIEW_H

#include <stdlib.h>

#include <kconfig.h>
#include <krandomsequence.h>

#include "frontend.h"
#include "kwritewords.h"

class MainWindow;

/**
 * This is the main view class for KMessedWords.  Most of the non-menu,
 * non-toolbar, and non-statusbar (e.g., non frame) GUI code should go
 * here.
 */

class KmwView : public FrontEnd
{
    Q_OBJECT
public:
  	///constructor
    	KmwView(MainWindow *parent=0, const char *name=0);
	///destructor
	virtual ~KmwView();
    
	///Holds the different levels
	QStringList m_levelString;
	 
	///language is the current language for data file
	QString language;
public slots:
    	///Each time the level changes, reset some parameters and restart game
    	virtual void slot_changeLevel(int);
	
private:

protected:
  	///Instance of the config class
  	KConfig *config;
	///A dictionary instance
	KWriteWords configInputData;
	///Get each word in random with a randomsequence instance
	KRandomSequence m_random;
	///The number of solved messed words for each
	int m_readSolved[10];
	///Number of rounds tried for each person in the 10 top scores
	int m_readRounds[10];
	///The guesses for each highscore
	int m_readGuesses[10];
	///The names of the 10 top players in scores
	QString m_readName[10];
	///Number of lines (=words) for the current level 
	int m_lines;
	///Number of attemps for guessing the answer right for a given word
	int m_guesses;
	///Number of words you guessed right
	int m_solvedWords;
	///Number of games you played (number of messed words you tried to solve)
	int m_messups;
	///Number of total guesses
	int m_tGuesses; 
	///Holds the messed word i.e. the word with the shuffled letters
	QString m_messedWord;
	///The word before it has been messed
	QString m_cleanWord;
	///Get randomly the clean word from the current level data file
	void randomizer(int level);
	///Shuffle the letters in the cleanWord to get the messedWord
	void messWord();
	///Get the files for each level
	void reloadStructure();
    	/** This is to reset the game.
	* This is called quite a few times, so to make it easier to make changes.
	* I added this to make it easier to maintain.
    	*/
	void resetKMW();
	
public slots:
  	///Shuffle the letters in the word
	virtual void slot_messup();

private slots:
  	///Assess wether the answer is right or wrong
	virtual void slot_try();
	///When you click Quit in the Answer dialog this slot is called, it displays your score and the application quits
	void slotEnd();
	///When you click Play Again in the Answer dialod this slot is called, it resets some variables and call sthe messup slot
	void slotReply();
	
  signals:
    	/** Emit a signal each time the level changes
   *   @param text the text (level name) hat will be written in the statusbar
     	 */
    	void changeLevel(const QString &text);
};

#endif // KMWVIEW_H
