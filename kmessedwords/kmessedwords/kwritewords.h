/***************************************************************************
                          kwritewords.h  -  description
                             -------------------
    begin                : Thu Aug 2 2001
    copyright            : (C) 2001 by Primoz Anzur 
    email                : zerokode@gmx.net
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef KWRITEWORDS_H
#define KWRITEWORDS_H

#include <qmultilineedit.h>
#include <qfile.h>
#include <kstandarddirs.h>
#include <klocale.h>

#include "kmwdict.h"

/**
  *@author Primoz Anzur (zer0Kode)
  */

class KWriteWords : public WordsEditor  {
   Q_OBJECT
public:
	KWriteWords(QWidget *parent=0, const char *name=0);
	~KWriteWords();

	void saveStreamToText();
	void openDataStream();

public slots:
	virtual void slotSaveWords();
	virtual void slotCancel();
	virtual void slotOK();

protected:
	QString kMessedDataDir;
	QString setLocalDictionary;

protected slots:
        void closeEvent (QCloseEvent *);

signals:
	void getClose();
};

#endif
