/***************************************************************************
                          kwritewords.cpp  -  description
                             -------------------
    begin                : Thu Aug 2 2001
    copyright            : (C) 2001 by Primoz Anzur 
    email                : zerokode@gmx.net
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "kwritewords.h"
#include "kwritewords.moc"

KWriteWords::KWriteWords(QWidget *parent, const char *name) : WordsEditor(parent,name, true)
{
	openDataStream();
}

KWriteWords::~KWriteWords()
{}

void KWriteWords::openDataStream()
{
	//This NEEDS to be included!! It can't refresh path the other way...
	setLocalDictionary=locate("data","kmessedwords/");
	QFile f(setLocalDictionary+"easy.txt");
	if (! f.exists())
	{
		setLocalDictionary=locate("data","kmessedwords/");
	}

	QFile openEasyFile(setLocalDictionary+"easy.txt");
	openEasyFile.open(IO_ReadOnly);
	QTextStream readEasyStr(&openEasyFile);
	easyDict->setText( I18N_NOOP(readEasyStr.read()) );
	openEasyFile.close();

	QFile openMediumFile(setLocalDictionary+"medium.txt");
	openMediumFile.open(IO_ReadOnly);
	QTextStream readMediumStr(&openMediumFile);
	mediumDict->setText( I18N_NOOP(readMediumStr.read()) );
	openMediumFile.close();

	QFile openHardFile(setLocalDictionary+"hard.txt");
	openHardFile.open(IO_ReadOnly);
	QTextStream readHardStr(&openHardFile);
	hardDict->setText( I18N_NOOP(readHardStr.read()) );
	openHardFile.close();
}

void KWriteWords::saveStreamToText()
{
	QFile easyFile(locateLocal("data","kmessedwords/easy.txt"));
	easyFile.open(IO_WriteOnly);
	QTextStream easyStr(&easyFile);
	easyStr << easyDict->text();
	easyFile.close();

	QFile mediumFile(locateLocal("data","kmessedwords/medium.txt"));
	mediumFile.open(IO_WriteOnly);
	QTextStream mediumStr(&mediumFile);
	mediumStr << mediumDict->text();
	mediumFile.close();

	QFile hardFile(locateLocal("data","kmessedwords/hard.txt"));
	hardFile.open(IO_WriteOnly);
	QTextStream hardStr(&hardFile);
	hardStr << hardDict->text();
	hardFile.close();

}

void KWriteWords::slotSaveWords()
{
	saveStreamToText();
	openDataStream();
}

void KWriteWords::slotOK()
{
	saveStreamToText();
	openDataStream();
	close();
}

void KWriteWords::slotCancel()
{
	close();
}

void KWriteWords::closeEvent(QCloseEvent *ev)
{
	emit getClose();
	hide();
}
