/***************************************************************************
                          main.cpp  -  description
                             -------------------
    begin                : Thu Jul 19 00:06:05 CEST 2001
    copyright            : (C) 2001 by Primoz Anzur
                         : (C) 2004 by Reuben Sutton 
    email                : zerokode@gmx.net
    email                : reubens@gmail.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include <kcmdlineargs.h>
#include <kaboutdata.h>
#include <kapplication.h>

#include "mainwindow.h"

static const char description[] =
	I18N_NOOP("Anagram game");

static KCmdLineOptions options[] =
{
    KCmdLineLastOption
};

int main(int argc, char *argv[])
{

  KAboutData aboutData( "kmessedwords", I18N_NOOP("KMessedWords"),
    LOCAL_VERSION, description, KAboutData::License_GPL,
    "(c) 2001-2004 Primoz Anzur\n(c) 2004- Reuben Sutton", 0, "http://edu.kde.org/kmessedwords", "submit@bugs.kde.org");
  aboutData.addAuthor("Reuben Sutton",I18N_NOOP("Maintainer"),"reubens@gmail.com");
  aboutData.addAuthor("Primoz Anzur",I18N_NOOP("Original Maintainer/Author"), "zerokode@gmx.net");
  aboutData.addAuthor("Anne-Marie Mahfouf",I18N_NOOP("Port to KConfig XT, clean code"), "annma@kde.org");
  aboutData.addCredit("Waldo Bastian",I18N_NOOP("Created the core KMW engine / improved syllables engine"), 
"bastian@kde.org");
  aboutData.addCredit("Chris Howells",I18N_NOOP("Added \"Help\" menu"), "howells@kde.org");
  aboutData.addCredit("David Vignoni", I18N_NOOP("svg icon"), "david80v@tin.it");
  aboutData.addCredit("Robbie Ward",I18N_NOOP("Various ideas, and suggestions"), "robbie.ward1@virgin.net");
  
  KCmdLineArgs::init( argc, argv, &aboutData );
  KCmdLineArgs::addCmdLineOptions( options ); // Add our own options.

  KApplication a;
  MainWindow *mw = new MainWindow();
  a.setMainWidget(mw);
  mw->show();

  return a.exec();
}
