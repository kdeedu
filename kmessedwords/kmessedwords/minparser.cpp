/***************************************************************************
                          minparser.cpp  -  description
                             -------------------
    begin                : Thu Mar 28 2002
    copyright            : (C) 2002 by Primoz Anzur 
    email                : zerokode@gmx.net
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
#include "minparser.h"
#include "kdebug.h"
QString data;
bool sort;

MinParser::MinParser()
{
}

MinParser::~MinParser()
{
}

QString MinParser::getSingleRow(int row)
{
	QStringList singleRow=QStringList::split("\n", data, true);
	QString rowData=singleRow[row];  //gets a complete line...
	int pos = rowData.find ("#"); //Commentary parser from Kevin... Wonderful idea!!!
	QString word;
	if (pos >-1)
	{
		word=rowData.left(pos);
	} else {
		word=rowData;
	}
	word=word.stripWhiteSpace();
	return word;
}

QString MinParser::getItem(int row, int item, const QString &delimiter)
{
	if (! getSingleRow(row))
		return 0L; //return null instantly, if the line is empty...
	
	singleItem=QStringList::split( delimiter, getSingleRow(row), true );
	return singleItem[item];
}

int MinParser::rows()
{
	QStringList allData=QStringList::split("\n", data, true);
	return allData.count();
}

int MinParser::objects(int row, const QString &delimiter)
{
	QStringList objData=QStringList::split(delimiter, getSingleRow(row));
	return objData.count();
}

void MinParser::loadFile(const QString &sFile)
{
	QFile openDataFile(sFile);
	openDataFile.open(IO_ReadOnly);
	QTextStream readDataStr(&openDataFile);
	data=readDataStr.read();
	openDataFile.close();
}

QString MinParser::getClean(int row, const QString &delimiter)
{
	QStringList objData=QStringList::split(delimiter, getSingleRow(row));
	QString saneData;
	for (int i=0; i < (int) objData.count(); i++)
	{
		QString sd=objData[i];
			saneData=saneData+sd;

		//OK... That was kind of a bad idea... We should actually
		//answer in normal word, not with syllables...
/*
		if (saneData.isEmpty())
			saneData=saneData+sd;
		else
			saneData=saneData+"-"+sd;
*/
	}
	return saneData;
}

QString MinParser::makeMess(int row, const QString &delimiter)
{
	QStringList objData=QStringList::split(delimiter, getSingleRow(row));
	QString insaneData;
	int count;
	
	// Stop when we have used up all chunks
	for (int i=0; (count = objData.count()); i++)
	{
		int objChunk;
		// If there is more than 1 chunk in total we make sure
		// that we never start with the begin-chunk.
		// This way the word will never be in the original order.
		if ((i == 0) && (count > 1))
		   objChunk = 1+random.getLong(count-1); // Use any chunk but the first one
		else
		   objChunk = random.getLong(count); // Use any chunk still left

		QStringList::Iterator it = objData.at(objChunk);
		QString sd = *it;
		objData.remove(it); // Remove chunk
		if (insaneData.isEmpty())
			insaneData = sd;
		else
			insaneData += "-" + sd;
	}
	return insaneData; // <- hehe... This is FUN :)
}
