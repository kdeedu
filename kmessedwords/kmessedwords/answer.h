/***************************************************************************
                          answer.h  -  description
                             -------------------
    begin                : Sun Oct 21 2001
    copyright            : (C) 2001 by Primoz Anzur 
    email                : zerokode@gmx.net
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ************************************************************************/
 
#ifndef ANSWER_H
#define ANSWER_H

#include <qdialog.h>
#include <qlayout.h>
class KPushButton;

class Answer : public QDialog
{ 
    Q_OBJECT

public:
    Answer( QWidget* parent = 0, const char* name = 0, bool modal = FALSE, WFlags fl = 0 );
    ~Answer();

    QPushButton* btnReplay;
    KPushButton* btnQuit;
	 bool getAnswer;

public slots:
    void exitAnswer();
    void replayGame();
	 void doAnswer(bool result);

protected:
    QGridLayout* AnswerLayout;
    QHBoxLayout* Layout2;

signals:
	void gameExit();
	void gameReplay();
};

#endif // ANSWER_H
