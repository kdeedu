/***************************************************************************
                          minparser.h  -  description
                             -------------------
    begin                : Thu Mar 28 2002
    copyright            : (C) 2002 by Primoz Anzur 
    email                : zerokode@gmx.net
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
#ifndef __MIN_PARSER_H
#define __MIN_PARSER_H

#include <qfile.h>
#include <qstringlist.h>
#include <time.h>
#include <stdlib.h>

#include <krandomsequence.h>

class MinParser {
	public:
	/*CTOR*/
	MinParser();
	/*DTOR*/
	~MinParser();
	
	QString getItem(int row, int item, const QString &delimiter);
	int rows();
	int objects(int row, const QString &delimiter);
	void loadFile(const QString &sFile);
	QString getSingleRow(int row);
	QString getClean(int row, const QString &delimiter);
	QString makeMess(int row, const QString &delimiter);
	QString loc;

protected:
	QString locatedLoc;
	QStringList singleItem;
	QString rowData;
	KRandomSequence random;
};

#endif
