// Error codes for the ApogeeUsb* files


#define APN_USB_SUCCESS				0

#define APN_USB_ERR_OPEN			1
#define APN_USB_ERR_READ			2
#define APN_USB_ERR_WRITE			3

#define APN_USB_ERR_IMAGE_DOWNLOAD	4

#define APN_USB_ERR_START_EXP		5
#define APN_USB_ERR_STOP_EXP		6

#define APN_USB_ERR_STATUS			7

#define APN_USB_ERR_RESET			8




